#![cfg_attr(not(feature = "std"), no_std)]

pub struct Dog {}
pub struct Cat {}

pub trait Speaks {
    fn speak(&self);
}

pub trait Animal {
fn animal_type(&self) -> &str;
fn noise(&self) -> &str;
}

impl<T> Speaks for T where T: Animal {
fn speak(&self) {
    //    println!("The {} said {}", self.animal_type(), self.noise());
}
}

impl Animal for Dog {
    fn animal_type(&self) -> &str {
        "dog"
    }
    
    fn noise(&self) -> &str {
        "woof"
    }
}

impl Animal for Cat {
    fn animal_type(&self) -> &str {
        "cat"
    }

    fn noise(&self) -> &str {
        "meow"
    }
}
